### NB: this was lifted from my masters thesis and contains mostly the same info

chromosomes = [str(x) for x in range(1, 23)]
chromosomes.append("x")
chromosomes.append("y")

chromosomes_small = ["21", "22", "y"]
chromosomes_smallish = ["4", "8", "9", "13", "14", "15", "16", "18", "19", "20", "21", "x", "y"]
chromosomes_big = [x for x in chromosomes if x not in chromosomes_small]
chromosomes_biggish = [x for x in chromosomes if x not in chromosomes_smallish]
chromosomes_autosomal = [str(x) for x in range(1, 23)]
chromosomes_allosomal = ["x", "y"]

methylation_array_norm_methods = ["raw", "noob", "swan", "quant", "funnorm", "illumina"]
methylation_array_types = ["EPIC", "450k"]

epic_probe_count = {  # protip for the RCC: wc -l ~/project/utility/*
    "1": 82013,
    "2": 64828,
    "3": 48896,
    "4": 36771,
    "5": 44720,
    "6": 54401,
    "7": 47560,
    "8": 38452,
    "9": 26167,
    "10": 42126,
    "11": 48894,
    "12": 44623,
    "13": 21040,
    "14": 29550,
    "15": 28741,
    "16": 37939,
    "17": 44435,
    "18": 14899,
    "19": 38550,
    "20": 22960,
    "21": 10300,
    "22": 18367,
    "x": 19090,
    "y": 537
}

epic_probes_smallest_to_largest = [x for x, v in sorted(list(epic_probe_count.items()), key=lambda z: z[1])]
epic_correlation_matrix_theoretical_size_in_bytes = {k: 8*v*v for k, v in epic_probe_count.items()}  # float64 = 8 bytes
appropriate_norm_type = {**{k: "noob" for k in chromosomes_autosomal}, **{k: "swan" for k in chromosomes_allosomal}, "autosomal": "noob"}  # based on my norm type study