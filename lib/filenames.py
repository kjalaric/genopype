"""
This file contains filepath formatting functions. The following convention applies:
* if information specific to a cohort is included, a full path is given unless the bool arg short_form is True, in which case only the base name is given
* if no information specific to a cohort is included, the short-form filename is given by default
'short_form' is included to help with consistency.
"""

import os
from configuration import Configuration

_methylation_beta_file = "beta_{}.csv"
_methylation_beta_chromosome_file = "chromosome_{}_betas_{}.csv"
_methylation_array_450k_cg_sites = "450k_sitecount_{}.txt"
_methylation_array_epic_cg_sites = "EPIC_sitecount_{}.txt"

_methylation_correlation = "{}_{}_{}{}_chr{}_{}.txt"  # cohort, corrtype, strength, positive/negative, chr, normtype
_methylation_correlation_pkl = "{}_{}_beta_chromosome_{}_{}.pkl"  # cohort, correlation type, chromosome, normtype







def methylation_beta_file(norm_type, cohort=None, short_form=False):
    if cohort is None or short_form is True:
        return _methylation_beta_file.format(norm_type)
    else:
        return os.path.join(Configuration.methylation_array_data_output, cohort, _methylation_beta_file.format(norm_type))


def methylation_beta_chromosome_file(norm_type, chromosome, cohort=None, short_form=False):
    if cohort is None or short_form is True:
        return _methylation_beta_chromosome_file.format(chromosome, norm_type)
    return os.path.join(Configuration.methylation_array_data_output, cohort, _methylation_beta_chromosome_file.format(chromosome, norm_type))


def methylation_array_cg_sites(chromosome, array_type, short_form=False):
    if array_type.upper() == "450K":
        shortfilepath = _methylation_array_450k_cg_sites.format(chromosome)
    elif array_type.upper() == "EPIC":
        shortfilepath = _methylation_array_450k_cg_sites.format(chromosome)
    else:
        raise Exception("Array type doesn't match any of the known ones.")
    if short_form:
        return shortfilepath
    else:
        return os.path.join(Configuration.methylation_array_probe_count_directory, shortfilepath)


def methylation_beta_correlation(corrtype, strength, direction, chromosome, normtype, cohort=None, short_form=False):
    if short_form is True:
        return _methylation_correlation.format(cohort, corrtype, strength, direction, chromosome, normtype)
    else:
        return os.path.join(Configuration.methylation_correlation_directory, _methylation_correlation.format(cohort, corrtype, strength, direction, chromosome, normtype))


def methylation_beta_correlation_pkl(corrtype, chromosome, normtype, cohort=None, short_form=False):
    if short_form is True:
        return _methylation_correlation_pkl.format(cohort, corrtype, chromosome, normtype)
    else:
        return os.path.join(Configuration.methylation_correlation_pkl_directory, _methylation_correlation_pkl.format(cohort, corrtype, chromosome, normtype))
