import os
import sys
import flask
import flask_cors
import flask_apscheduler
import werkzeug
import json
from configuration import Configuration
import gpbackend.upload_handler
import gpbackend.pipeline

app = flask.Flask(__name__)
cors = flask_cors.CORS(app, resources = {
    r"/api/upload": {"origins": "*"},
    r"/api/listdir": {"origins": "*"},
    r"/api/pipeline": {"origins": "*"}

})
scheduler = flask_apscheduler.APScheduler()
scheduler.init_app(app)
scheduler.start()

@app.route('/api', methods=["GET", "POST"])
def gpServer():
    print("fff", file=sys.stderr)

    if flask.request.method == "GET":
        return gpserverGet()
    elif flask.request.method == "POST":
        return gpserverPost()


# http://localhost:5000/api?name1=test&name2=haha
def gpserverGet():
    return flask.make_response(
        flask.jsonify({**{"words": "Everything is going to be ok", "method":flask.request.method}, **{k: v for k, v in flask.request.args.items()}}),
        200
    )


def gpserverPost():
    pass


@app.route('/api/upload', methods=["POST", "GET"])
def gpUpload():
    print(len(flask.request.files), file=sys.stderr)
    f = flask.request.files['file']
    file_path = os.path.join(Configuration.backend_upload_directory, werkzeug.utils.secure_filename(f.filename))
    f.save(file_path)
    scheduler.add_job(
        func=gpbackend.upload_handler.fileHandler,
        args=[file_path],
        id=f'uploadFile_{file_path}'
    )
    return 'OK'


@app.route('/api/listdir', methods=["GET"])
def gpListdir():
    uploads = os.listdir(Configuration.backend_upload_directory)
    if len(uploads) == 0:
        uploads = ["(nothing found)"]
    downloads = os.listdir(Configuration.backend_download_directory)
    if len(downloads) == 0:
        downloads = ["(nothing found)"]

    return flask.make_response(
        flask.jsonify({
            "uploads": uploads,
            "downloads": downloads,
        }),
        200
    )


@app.route('/api/listdir/methyl-unprocessed', methods=["GET"])
def gpListMethylUnprocessed():
    # projects = [x for x in os.listdir(Configuration.backend_source_directory) if os.path.isdir(x)]  # todo explictly identify methylation projects
    projects = [x.path for x in os.scandir(Configuration.backend_source_directory) if x.is_dir()]  # todo explictly identify methylation projects


    return flask.make_response(
        flask.jsonify({
            "projects": projects,
        }),
        200
    )


@app.route('/api/pipeline', methods=["POST", "GET"])
def gpStartPipeline():
    """
    Request must include:
    * "pipeline": pipeline_id
    * "project": project_id
    * "parameters": {parameters relevant to the pipeline being used}
    :return:
    """
    print(json.loads(flask.request.data), file=sys.stderr)
    request_data = json.loads(flask.request.data)
    scheduler.add_job(
        func=gpbackend.pipeline.execute,
        args=[request_data],
        id=f'startPipeline_{request_data["pipeline"]}_{request_data["project"]}'
    )

    return 'OK'
