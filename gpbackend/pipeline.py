"""
Most of these will simply invoke luigi via the command line.
The server provides information regarding the request in dict format (from jsons).
"""
import subprocess
import sys
import luigi
from configuration import Configuration
import lib.bio
import tasklib.methylation_microarray


def execute(request_data):
    pipeline = request_data['pipeline']
    project = request_data['project']
    parameters = request_data['parameters']

    if pipeline == "microarrayPreprocessing":
        microarrayPreprocessing(project, parameters)
    elif pipeline == 'microarrayCorrelations':
        microarrayCorrelations(project, parameters)


def microarrayPreprocessing(project, parameters):
    try:
        norm_type = parameters["norm_type"].lower()
        array_type = parameters["array_type"]
    except KeyError as e:
        print(e, file=sys.stderr)

    if norm_type not in lib.bio.methylation_array_norm_methods:
        raise Exception(f"Unrecognised norm type: {norm_type}")
    if array_type not in lib.bio.methylation_array_types:
        raise Exception(f"Unrecognised array type: {array_type}")

    # subprocess.run(f"{Configuration.python_executable} -m luigi --module main DivideBetaCsvIntoChromosomes --cohort {project} --normtype {norm_type} --arraytype {array_type} --local-schedule")
    # print(f"{Configuration.python_executable} -m luigi --module main DivideBetaCsvIntoChromosomes --cohort {project} --normtype {norm_type} --arraytype {array_type} --local-schedule", file=sys.stderr)

    luigi.build([tasklib.methylation_microarray.DivideBetaCsvIntoChromosomes(cohort=project, normtype=norm_type, arraytype=array_type)], local_scheduler=True)


def microarrayCorrelations(project, parameters):
    try:
        norm_type = parameters["norm_type"].lower()
        array_type = parameters["array_type"]
        chromosome = parameters["chromosome"].lower()
        corr_type = parameters["corr_type"].lower()
    except KeyError as e:
        print(e, file=sys.stderr)

    if norm_type not in lib.bio.methylation_array_norm_methods:
        raise Exception(f"Unrecognised norm type: {norm_type}")
    if array_type not in lib.bio.methylation_array_types:
        raise Exception(f"Unrecognised array type: {array_type}")

    # subprocess.run(f"{Configuration.python_executable} -m luigi --module main DivideBetaCsvIntoChromosomes --cohort {project} --normtype {norm_type} --arraytype {array_type} --local-schedule")
    # print(f"{Configuration.python_executable} -m luigi --module main DivideBetaCsvIntoChromosomes --cohort {project} --normtype {norm_type} --arraytype {array_type} --local-schedule", file=sys.stderr)

    luigi.build([tasklib.methylation_microarray.CalculateMethylationCorrelation(cohort=project, normtype=norm_type, chromosome=chromosome, corrtype=corr_type, arraytype=array_type)], local_scheduler=True)