import sys
from datetime import datetime


def Log(string):  # uppercase because lowercase is overriden by imports, also this should be a class
    print(f"{datetime.now().strftime('%H:%M:%S')}| {string}", file=sys.stderr)


if __name__ == "__main__":
    Log("test")