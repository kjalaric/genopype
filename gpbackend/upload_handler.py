"""
Handles files uploaded to the server.
"""
import os
import shutil
import zipfile
import time
from configuration import Configuration
from gpbackend.logging import Log

allowable_file_types = [
    ".zip"
]


def fileHandler(file_location):
    '''
    Handles files as they are downloaded or uploaded into the server.

    Zip files: if files have already been uploaded, the old ones are replaced. Current
    logic for this is that if a directory with the same name of the zip files exists,
    it is deleted.

    :param file_location:
    :return: None
    '''
    if not os.path.exists(file_location):
        raise Exception("File does not exist.")  # should probably return something to the frontend.

    file_name, file_type = os.path.splitext(os.path.basename(file_location))
    output_directory = os.path.join(Configuration.backend_source_directory, file_name)

    if os.path.isdir(output_directory):
        Log(f"Deleting existing {output_directory}")
        shutil.rmtree(output_directory)
        time.sleep(0.1)  # will get permission errors if we don't wait for a bit after deleting

    file_type = file_type.lower()
    if file_type not in allowable_file_types:
        raise Exception("Couldn't handle this file.")  # should probably return something to the frontend.
    else:
        os.mkdir(output_directory)

    if file_type.lower() == ".zip":
        Log(f"Started extracting {file_location}")
        with zipfile.ZipFile(file_location, "r") as zf:
            zf.extractall(output_directory)
        Log(f"Zip file extracted to {output_directory}")
        os.remove(file_location)
        return True




    return False

