import Head from 'next/head'
import Link from 'next/link'
import styles from '/styles/Home.module.css'

export default function DataImporter() {
    return (
        <div className = {styles.container}>
        <div className={styles.main}>
    <Head>
        <title>Genopype Dev Frontend</title>
        <meta name="description" content="Dev frontend." />
        <link rel="icon" href="/favicon2.png" />
    </Head>
    <h1>Data Importer</h1>
    <h2>Please note that this feature is in development and will not work with much beyond NCBI GEO Accessions at this point in time.</h2>
    <h2>
    <Link href="/">
            <a className={styles.textlink}>Back to the home page. </a>
        </Link>
    </h2>
    <a href="/" className={styles.card}
          >
            <h2>Back</h2>
            <p>Return to the homepage.</p>
          </a>

          <a href="/tools/workbench/upload" className={styles.card}
          >
            <h2>File Upload</h2>
            <p>Upload files to the server.</p>
          </a>

    </div>
    </div>
    )
  }
  