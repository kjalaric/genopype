import Head from 'next/head'
import Link from 'next/link'
import styles from '/styles/Home.module.css'
import React, {useState} from 'react';
const axios = require('axios').default;

export default function FileViewer() {
    const [uploads, setUploads] = useState();
    const [downloads, setDownloads] = useState();
    const [requestComplete, setRequestComplete] = useState(false)
    
    const getListdir = () => {
        axios.request({
            method: "get",
            url: "http://localhost:5000/api/listdir",
            headers: {"Content-Type": "application/json"}
            })
            //.then((response) => response.json())
			.then((result) => {
                console.log('Success:', result);
                setUploads(result.data.uploads)
                setDownloads(result.data.downloads)
                setRequestComplete(true)
			})
			.catch((error) => {
				console.error('Error:', error);
			});
            
    };

    return (

        <div className = {styles.container}>
        <div className={styles.main}>
    <Head>
        <title>Genopype Dev Frontend</title>
        <meta name="description" content="Dev frontend." />
        <link rel="icon" href="/favicon2.png" />
    </Head>
    <h1>File Viewer</h1>
    <h2>
    <Link href="/">
            <a className={styles.textlink}>Back to the home page.</a>
        </Link>
    </h2>
    <div>
        <button onClick={getListdir}>What's inside?</button>
        {requestComplete ? (
            <div>
                <p>Downloads:</p>
                {
                    downloads.map((name, index) => {
                    return <div>{name}</div>
                    })
                }
                <p>Uploads:</p>
                {
                    uploads.map((name, index) => {
                    return <div>{name}</div>
                    })
                }
            </div>
        ) : (<></>
        )
        
    }
    </div>
    </div></div>
    )
  }
  