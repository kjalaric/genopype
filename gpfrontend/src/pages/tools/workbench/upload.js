import Head from 'next/head'
import Link from 'next/link'
import styles from '/styles/Home.module.css'
import FileUploadPage from '/src/components/fileupload'


export default function WorkbenchUpload() {
    return (
    <div className = {styles.container}>
    <div className={styles.main}>
    <Head>
        <title>Genopype Dev Frontend</title>
        <meta name="description" content="Dev frontend." />
        <link rel="icon" href="/favicon2.png" />
    </Head>
    <h1>File Uploader</h1>
    <div>
        The pipeline can accept the following formats:
        <ul>
            <li>.zip</li>
        </ul>
    </div>
    <Link href="/tools/workbench">
            <a className={styles.textlink}>Back to the workbench.</a>
    </Link>

    <FileUploadPage></FileUploadPage>
    </div>
    </div>
    )
  }
  