import Head from 'next/head'
import Link from 'next/link'
import styles from '/styles/Home.module.css'


export default function Pipelines() {
    return (
    <div className = {styles.container}>
    <div className={styles.main}>
    <h1 className={styles.title}>Pipelines</h1>

    <div className={styles.grid}>

          <a href="/tools/workbench" className={styles.card}
          >
            <h2>Back</h2>
            <p>Return to the workbench.</p>
          </a>

          <a href="/tools/workbench/pipelines/methylation_array" className={styles.card}
          >
            <h2>Illumina Methylation Array Processing</h2>
            <p>Convert IDAT files from MethlyationEPIC Beadchip (863k+ sites) or HumanMethylation450 Beadchip (450k+ sites) into methylation intensities using selected normalisation methods.</p>
          </a>

        </div>
    </div>
    </div>
    )
  }
  