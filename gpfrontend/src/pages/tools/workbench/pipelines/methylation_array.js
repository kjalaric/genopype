import Head from 'next/head'
import Link from 'next/link'
import styles from '/styles/Home.module.css'
import React, {useState} from 'react';
const axios = require('axios').default;


export default function MethylationArraySelection() {
    const [projects, setProjects] = useState();
    const [requestComplete, setRequestComplete] = useState();

    const handlePreprocessingSubmission = () => {
        axios.request({
            method: "post",
            url: "http://localhost:5000/api/pipeline",
            data: {
                "pipeline": "microarrayPreprocessing",
                "project": "E-MTAB-7069",
                "parameters": {
                    "norm_type": "swan",
                    "array_type": "EPIC"
                }
            
            },
            headers: {
                'Content-Type': 'application/json'
                },

            }
            )
            .then((response) => {
                setServerResponse(response)
                setUploadComplete(true)
            })
            .then((result) => {
                console.log('Success:', result);
            })
            .catch((error) => {
                console.error('Error:', error);
            });
        };

        const handleCorrelationSubmission = () => {
            axios.request({
                method: "post",
                url: "http://localhost:5000/api/pipeline",
                data: {
                    "pipeline": "microarrayCorrelations",
                    "project": "E-MTAB-7069",
                    "parameters": {
                        "norm_type": "swan",
                        "array_type": "EPIC",
                        "chromosome": "21",
                        "corr_type": "spearman"
                    }
                
                },
                headers: {
                    'Content-Type': 'application/json'
                    },
    
                }
                )
                .then((response) => {
                    setServerResponse(response)
                    setUploadComplete(true)
                })
                .then((result) => {
                    console.log('Success:', result);
                })
                .catch((error) => {
                    console.error('Error:', error);
                });
            };

	return(
        <div className={styles.container}>
        <Head>
          <title>Genopype Dev Frontend</title>
          <meta name="description" content="Dev frontend." />
          <link rel="icon" href="/favicon2.png" />
        </Head>
  
        <main className={styles.main}>
        <div>
                 <div>
                     <button onClick={handlePreprocessingSubmission}>Preprocessing</button>
                 </div>
                 <br></br>
                 {requestComplete ? (
                     <div>
                         <p>Request Complete!</p>
                     </div>
                 ) : (
                     <></>
                 )
                 }
             </div>
             <div>
                 <div>
                     <button onClick={handleCorrelationSubmission}>Correlations</button>
                 </div>
                 <br></br>
                 {requestComplete ? (
                     <div>
                         <p>Request Complete!</p>
                     </div>
                 ) : (
                     <></>
                 )
                 }
             </div>
         </main>
         </div>
    )}