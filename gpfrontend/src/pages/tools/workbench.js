import Head from 'next/head'
import Link from 'next/link'
import styles from '/styles/Home.module.css'
import FileUploadPage from '/src/components/fileupload'



export default function DataWorkbench() {
    return (
    <div className = {styles.container}>
    <div className={styles.main}>
    <div className={styles.grid}>

          <a href="/" className={styles.card}
          >
            <h2>Back</h2>
            <p>Return to the homepage.</p>
          </a>

          <a href="/tools/workbench/upload" className={styles.card}
          >
            <h2>File Upload</h2>
            <p>Upload files to the server.</p>
          </a>
 
          <a href="/tools/workbench/file_viewer" className={styles.card}
          >
            <h2>File Viewer</h2>
            <p>View files that have been uploaded to or downloaded by the server.</p>
          </a>

          <a href="/tools/workbench/pipelines" className={styles.card}
          >
            <h2>Pipelines</h2>
            <p>Try and push your data through some magic internet pipes.</p>
          </a>

        </div>
    </div>
    </div>
    )
  }
  