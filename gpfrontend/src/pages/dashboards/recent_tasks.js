import Head from 'next/head'
import Link from 'next/link'
import styles from '/styles/Home.module.css'

export default function DashboardRecentTasks() {
    return (
        <div className = {styles.container}>
        <div className={styles.main}>
    <Head>
        <title>Genopype Dev Frontend</title>
        <meta name="description" content="Dev frontend." />
        <link rel="icon" href="/favicon2.png" />
    </Head>
    <h1>Dashboard - Recent Tasks</h1>
    <h2>Not much is happening here. <br></br>
    <Link href="/">
            <a className={styles.textlink}>Back to the home page.</a>
        </Link>
    </h2>
    </div>
    </div>
    )
  }
  