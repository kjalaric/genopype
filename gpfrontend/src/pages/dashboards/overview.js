import Head from 'next/head'
import Link from 'next/link'
import styles from '/styles/Home.module.css'

export default function DashboardOverview() {
    return (
        <div className = {styles.container}>
        <div className={styles.main}>
    <Head>
        <title>Genopype Dev Frontend</title>
        <meta name="description" content="Dev frontend." />
        <link rel="icon" href="/favicon2.png" />
    </Head>
    <h1>Dashboard - Overview</h1>
    <h2>Just kidding.<br></br>
    <Link href="/">
            <a className={styles.textlink}>Back to the home page.</a>
        </Link>
    </h2>
    </div>
    </div>
    )
  }
  