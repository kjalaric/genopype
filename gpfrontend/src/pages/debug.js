import Head from 'next/head'
import Image from 'next/image'
import styles from '/styles/Home.module.css'


export default function DebugMenu() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Genopype Dev Frontend</title>
        <meta name="description" content="Dev frontend." />
        <link rel="icon" href="/favicon2.png" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          This is a website!
        </h1>

        <p className={styles.description}>
          It's actually only a dev build.
        </p>

        <div className={styles.grid}>
          <a href="/" className={styles.card}
          >
            <h2>Main page</h2>
          </a>

          <a href="/tools/workbench/pipelines/methylation_array" className={styles.card}
          >
            <h2>Methylation Array</h2>
          </a>




        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://google.co.nz"
          target="_blank"
          rel="noopener noreferrer"
        >
          Something something 2021
        </a>
      </footer>
    </div>
  )
}
