import React from 'react';
import Head from 'next/head'
import styles from '/styles/Home.module.css'


export default function App() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Genopype Dev Frontend</title>
        <meta name="description" content="Dev frontend." />
        <link rel="icon" href="/favicon2.png" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          This is a website!
        </h1>

        <p className={styles.description}>
          It's actually only a dev build.
        </p>

        <div className={styles.grid}>
          <a href="/dashboards/overview" className={styles.card}
          >
            <h2>Overview</h2>
            <p>Everyone loves dashboards.</p>
          </a>

          <a href="/tools/workbench" className={styles.card}
          >
            <h2>Data Workbench</h2>
            <p>Do things with your data.</p>
          </a>

          <a href="/tools/data_importer" className={styles.card}
          >
            <h2>Data Importer</h2>
            <p>Upload data or pull from other data sources.</p>
          </a>

          
          <a href="/tools/automation_planner" className={styles.card_unimplemented}
          >
            <h2>Automation Planner</h2>
            <p>Setting yourself up for failure.</p>
          </a>

          <a href="/dashboards/recent_failures" className={styles.card_unimplemented}
          >
            <h2>Recent Failures</h2>
            <p>This is probably your fault.</p>
          </a>

          <a href="/dashboards/recent_tasks" className={styles.card_unimplemented}
          >
            <h2>Recent Tasks</h2>
            <p>The failures are still your fault.</p>
          </a>

          <a href="/tools/export_results" className={styles.card_unimplemented}
          >
            <h2>Results Exporter</h2>
            <p>Export evidence of your failures.</p>
          </a>

          <a href="/comms/support" className={styles.card_unimplemented}
          >
            <h2>Help & Support</h2>
            <p>Go crying to the developer.</p>
          </a>

          <a href="/debug" className={styles.card}
          >
            <h2>Debug Menu</h2>
            <p>Cheat codes for front end development.</p>
          </a>

        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://google.co.nz"
          target="_blank"
          rel="noopener noreferrer"
        >
          Something something 2021
        </a>
      </footer>
    </div>
  )
}
