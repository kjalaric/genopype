//https://www.pluralsight.com/guides/uploading-files-with-reactjs

import React, {useState} from 'react';

const axios = require('axios').default;

export default function FileUploadPage(){  // should this be default? i am bad at javascript
	const [selectedFile, setSelectedFile] = useState();
    const [isFilePicked, setIsFilePicked] = useState(false);
    const [isSelected, setIsSelected] = useState(false);
    const [progress, setProgress] = useState()
    const [barAdded, setBarAdded] = useState()
    const [uploadComplete, setUploadComplete] = useState()
    
	const changeHandler = (event) => {
		setSelectedFile(event.target.files[0]);
		setIsSelected(true);
	};

	const handleSubmission = () => {
		const formData = new FormData();
        formData.append('file', selectedFile);
        setBarAdded(true);
        setUploadComplete(false)

		axios.request({
            method: "post",
            url: "http://localhost:5000/api/upload",
            data: formData,
            headers: {
                'Content-Type': 'multipart/form-data'
                },
            onUploadProgress: (progressEvent) => {
                const progress = parseInt(Math.round((progressEvent.loaded*100)/progressEvent.total));
                setProgress(progress)
                if (progressEvent.loaded == progressEvent.total) {
                    setUploadComplete(true)
                    setBarAdded(false)
                }

                }   
            }
            )
			.then((response) => response.json())
			.then((result) => {
				console.log('Success:', result);
			})
			.catch((error) => {
				console.error('Error:', error);
			});
	};

	return(
   <div>
			<input type="file" name="file" onChange={changeHandler} />
			{isSelected ? (
				<div>
					<p>Filename: {selectedFile.name}</p>
					<p>Filetype: {selectedFile.type}</p>
					<p>Size in bytes: {selectedFile.size}</p>
				</div>
			) : (
				<p>Select a file to show details</p>
			)}
			<div>
				<button onClick={handleSubmission}>Submit</button>
			</div>
            <br></br>
            {uploadComplete ? (
                <div>
                    <p>Upload Complete!</p>
                </div>
            ) : (
                <></>
            )

            }
            {barAdded ? (
                <div>
                    <label for="file">Upload progress:  </label>
                    <progress id="file" value={progress} max="100"></progress>
                </div>
            ) : (
                <></>
            ) }

		</div>
	)
}