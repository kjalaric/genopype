//https://www.pluralsight.com/guides/uploading-files-with-reactjs

import React, {useState} from 'react';

const axios = require('axios').default;

export default function StartPipeline(){  // should this be default? i am bad at javascript
    const [uploadComplete, setUploadComplete] = useState()
    const [serverResponse, setServerResponse] = useState()
    
	const getProjects = () => {
        setUploadComplete(false)

        axios.request({
            method: "get",
            url: "http://localhost:5000/api/listdir/methyl-unprocessed",
            headers: {"Content-Type": "application/json"}
            })
            .then((response) => {
                setProjects(response.data.projects)
                setRequestComplete(true)
                console.log(projects)
                // console.log('Success:', result);
            })
			.catch((error) => {
				console.error('Error:', error);
			});

    };

    setRequestComplete(false)
    getProjects();


    return (

    <div className = {styles.container}>
    <div className={styles.main}>

    <h1 className={styles.title}>Methylation Array Processing</h1>
    <a href="/tools/workbench/pipelines" className={styles.card}
        >
        <h2>Back</h2>
        <p>Return to Pipelines.</p>
        </a>
    <h2>
        Select a victim to sacrifice to the pipeline gods:
    </h2>

    <div>
    {requestComplete ? (
        <div>
            {
                projects.map((name, index) => {
                return <div>{name}</div>
                })
            }
        </div>
    ) : (<div>Loading...</div>)}
    </div>

    </div>
    </div>
    )
  }
  

