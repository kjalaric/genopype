import os
import subprocess
import luigi
import luigi.contrib.external_program
from configuration import Configuration
import tasklib.directory_management
import application_scripts.methylation_array_wrangling
import application_scripts.methylation_correlation
import lib.filenames


class ProcessMethylationArraySWAN(luigi.contrib.external_program.ExternalProgramTask):
    cohort = luigi.Parameter()
    # array_type = luigi.Parameter

    def requires(self):
        return tasklib.directory_management.DirectoryCheckAndSetup(directory=os.path.join(Configuration.methylation_array_data_output, self.cohort))

    def output(self):
        return luigi.LocalTarget(os.path.join(Configuration.methylation_array_data_output, self.cohort, "beta_swan.csv"))

    def program_args(self):
        return ['Rscript', "./application_scripts/illumina_methylation.R", self.cohort, "swan"]


class DivideBetaCsvIntoChromosomes(luigi.Task):
    cohort = luigi.Parameter()
    normtype = luigi.Parameter()
    arraytype = luigi.Parameter()
    # droplist = luigi.ListParameter(default=list())

    def requires(self):
        return ProcessMethylationArraySWAN(self.cohort)

    def run(self):
        application_scripts.methylation_array_wrangling.divideCSVIntoChromosomes(self.cohort, self.normtype, self.arraytype)

    def output(self):
        return luigi.LocalTarget(lib.filenames.methylation_beta_chromosome_file(self.normtype, "1", self.cohort))


class CalculateMethylationCorrelation(luigi.Task):
    cohort = luigi.Parameter()
    chromosome = luigi.Parameter()
    normtype = luigi.Parameter()
    corrtype = luigi.Parameter()
    arraytype = luigi.Parameter()  # to resolve dependencies

    def requires(self):
        return DivideBetaCsvIntoChromosomes(self.cohort, self.normtype, self.arraytype)

    def run(self):
        application_scripts.methylation_correlation.generateMethylationCorrelationFiles(
            self.cohort, self.chromosome, self.normtype, self.corrtype)

    def output(self):
        luigi.LocalTarget(lib.filenames.methylation_beta_correlation(
            self.corrtype, "strong", "positive", self.chromosome, self.normtype, self.cohort))  # chromosome y is the last one it will get to
