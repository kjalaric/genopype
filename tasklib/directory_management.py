import os
import luigi


class DirectoryCreated(luigi.target.Target):
    def __init__(self, dir_loc):
        # super.__init__()  # doesn't appear to be an __init__ in luigi.target.Target
        self.dir_loc = dir_loc

    def exists(self):
        return os.path.exists(self.dir_loc)


class DirectoryCheckAndSetup(luigi.Task):
    directory = luigi.Parameter()

    def run(self):
        if not os.path.isdir(self.directory):
            if not os.path.exists(self.directory):
                os.makedirs(self.directory)
            else:
                raise Exception("Tried to create a directory with a name already in use.")

    def output(self):
        return DirectoryCreated(self.directory)