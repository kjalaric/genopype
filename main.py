import os
from configuration import Configuration
import tasklib.methylation_microarray

if __name__ == "__main__":
    dirr = os.path.join(Configuration.methylation_array_data_output, "E-MTAB-7069", "beta_swan.csv")
    dirr2 = "K:/automation_testing/output/E-MTAB-7069/beta_swan.csv"
    dir_base = os.path.join(Configuration.methylation_array_data_output, "E-MTAB-7069")
    dir3 = "K:\\automation_testing\\output\\E-MTAB-7069"
    print(dir_base)
    print(os.path.exists(dirr2))

    print(os.listdir(dir3))


'''
# luigi example
class GenerateWords(luigi.Task):
    def output(self):
        return luigi.LocalTarget('words.txt')

    def run(self):
        # write a dummy list of words to output file﻿
        words = ['apple', 'banana','grapefruit']
        with self.output().open('w') as f:
            for word in words:
                f.write('{word}\n'.format(word=word))


class CountLetters(luigi.Task):
    def requires(self):
        return GenerateWords()

    def output(self):
        return luigi.LocalTarget('letter_counts.txt')

    def run(self):
        # read in file as list﻿
        with self.input().open('r') as infile:
            words = infile.read().splitlines()
        # write each word to output file with its corresponding letter count﻿
        with self.output().open('w') as outfile:
            for word in words:
                outfile.write('{word} | {letter_count}\n'.format(word=word, letter_count=len(word)))
'''

