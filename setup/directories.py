import os
from configuration import Configuration


if __name__ == "__main__":
    for x in Configuration.__dict__:
        if x[0:2] != "__":
            loc = getattr(Configuration, x)
            if type(loc) is str:
                # case 1: directory exists
                if os.path.isdir(loc):
                    continue
                # case 2: file exists
                elif os.path.isfile(loc):
                    continue
                # case 3: otherwise, generate directory
                else:
                    print(f"Making new directory: {loc}")
                    os.makedirs(loc)

