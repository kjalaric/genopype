import os
import pandas as pd
import lib.bio
import lib.filenames


def divideCSVIntoChromosomes(cohort, norm_type, array_type, droplist=None):
    beta_mat = pd.read_csv(lib.filenames.methylation_beta_file(norm_type, cohort), index_col=0)
    if droplist:
        beta_mat.drop(droplist, axis=1, inplace=True)  # drop columns
    for chromosome in lib.bio.chromosomes:
        with open(lib.filenames.methylation_array_cg_sites(chromosome, array_type), "r") as fi:
            sites = [x.strip() for x in fi.readlines()]
        beta_mat.loc[sites].to_csv(lib.filenames.methylation_beta_chromosome_file(norm_type, chromosome, cohort))
        print("Wrote file for: {} | {}".format(norm_type, chromosome))
