import os
import pandas as pd
from components.correlation_writer import Writer
import lib.filenames
from configuration import Configuration


def generateMethylationCorrelationFiles(cohort, chromosome, normtype,
        corrtype=Configuration.App.methylation_correlation["default_correlation_type"],
        threshold=Configuration.App.methylation_correlation["basic_strong_correlation_threshold"],
        generate_pkls=True, pkls_only=False, regenerate=False):
    print("Starting {} chr {}: {}...".format(cohort, chromosome, normtype))
    pkl_file = lib.filenames.methylation_beta_correlation_pkl(corrtype, chromosome, normtype, cohort)

    try:
        if regenerate:
            raise FileNotFoundError  # this is easier
        beta_corr = pd.read_pickle(pkl_file)
        print(f"Found pkl file: {pkl_file}")
    except FileNotFoundError:
        print("Generating correlation pkl file...")
        df = pd.read_csv(lib.filenames.methylation_beta_chromosome_file(normtype, chromosome, cohort=cohort, short_form=False), index_col=0)
        df = df.T
        beta_corr = df.corr(method=corrtype)
        if generate_pkls:
            beta_corr.to_pickle(pkl_file)

    if pkls_only:
        return

    writer = Writer(cohort, chromosome, normtype)

    x_size = beta_corr.shape[0]  # rows
    y_size = beta_corr.shape[1]  # columns
    if x_size != y_size:
        raise Exception(f"Correlation matrix dimensions are different: {x_size}, {y_size}")

    # searches through the lower triangle
    index = 1
    ct = 1

    for cpg1, row in df.iterrows():
        for cpg2, x in row.iteritems():
            if ct >= index:
                break
            ct += 1
            if x >= threshold:
                writer.writeCorrelation(True, cpg1, cpg2, x)
            elif x <= -threshold:
                writer.writeCorrelation(False, cpg1, cpg2, x)
        ct = 1
        index += 1

    writer.exit()
