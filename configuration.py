import os
import json
import operator

config_files = [  # these should be in the main directory
    "loc_config.json",
]

app_config_files = [  # these should be in the main directory
    'app_config.json'
]


# global configuration singleton
class Configuration:
    __instance = None

    '''
    @staticmethod
    def getInstance():
        if Configuration.__instance is None:
            Configuration()
        return Configuration.__instance
    '''
    class App:
        pass


    def __init__(self):
        Configuration.__instance = self
        self.App = self.App()


Configuration()


def _unpack_config_path_recursive(_k, _v, dict_reference, directory_keys):
    if _v is dict:
        yield _unpack_config_path_recursive(_k, _v, dict_reference, [*directory_keys, _k[1:]])
    else:
        yield _k, _v


def _unpack_config_path(_k, _v, dict_reference):
    print(_k, _v)

    config_paths = dict()
    if _v is dict:
        for _kk, _vv in _unpack_config_path_recursive(_k, _v, dict_reference, [_k[1:]]):  #
            config_paths[_kk] = _vv
    else:
        config_paths[_k] = _v
    return config_paths.items()


def _dict_flattener(dict_in, flat=list()):
    if isinstance(dict_in, dict):
        for key, value in dict_in.items():
            if isinstance(value, dict):
                for d in _dict_flattener(value, flat + [key]):
                    yield d
            elif isinstance(value, list) or isinstance(value, tuple):
                for v in value:
                    for d in _dict_flattener(v, flat + [key]):
                        yield d
            else:
                yield flat + [key, value]
    else:
        yield flat + [dict_in]


# APP CONFIG - no more than one nested dict.
for config_file_path in [os.path.join(os.path.dirname(__file__), x) for x in app_config_files]:
    with open(config_file_path, "r") as fi:
        config_items = json.load(fi)
    for k, v in config_items.items():
        if v is dict:
            # check if its already there, in case the app was defined in a different json
            try:
                getattr(Configuration.App, k)
            except AttributeError:
                setattr(Configuration.App, k, type(k, (), {}))
            for kk, vv in v.items():
                setattr(Configuration.App.k, kk, vv)
        else:
            setattr(Configuration.App, k, v)


# LOCATION CONFIG
# pre-start checks - make sure all required config files are in the main directory
for config_file_path in [os.path.join(os.path.dirname(__file__), x) for x in config_files]:
    if not os.path.exists(config_file_path):
        raise Exception("Required config file does not exist: ", config_file_path)
    else:
        with open(config_file_path, "r") as fi:
            config_items = json.load(fi)
        flattened_config = list(_dict_flattener(config_items))

    paths = dict()
    subdirectories = set()

    # identify non-terminal subdirectories first. also remove them from the list
    for x in flattened_config:
        for y in x:
            if y[0] == "_":
                subdirectories.add(y[1:])

    # pull out entries where these subdirectories are defined
    subdirectory_entries = list()
    remove_these_entries = list()
    for i, x in enumerate(flattened_config):
        if x[-2] in subdirectories:
            subdirectory_entries.append(x)
            remove_these_entries.append(i)
    flattened_config = [z for zz, z in enumerate(flattened_config) if zz not in remove_these_entries]

    # resolve subdirectory paths
    # subdirectory_entries_organised = {x[0]: x[1] for x in subdirectory_entries if len(x) == 2}
    subdirectory_entries_organised = dict()
    paths = {x[0]: x[1] for x in subdirectory_entries if len(x) == 2}

    # resolve 'easy' paths that still remain
    remove_these_entries = list()
    for i, x in enumerate(flattened_config):
        if len(x) == 2:
            paths[x[0]] = x[1]
            remove_these_entries.append(i)
    flattened_config = [x for i, x in enumerate(flattened_config) if i not in remove_these_entries]

    # condense down the 'hard' ones
    for x in [z for z in subdirectory_entries if len(z) > 2]:
        subdirectory_entries_organised[x[-2]] = [x[-3], x[-1]]# [z for i, z in enumerate(x) if (i != len(x)-2)]  #  and len(x) > 2

    # replace _xxx with the actual path and os.path.join the result, adding this to paths
    while len(subdirectory_entries_organised):
        added_values = list()
        for k, v in subdirectory_entries_organised.items():
            try:
                paths[k] = os.path.join(paths[v[0][1:]], v[1])
                added_values.append(k)
            except Exception as e:
                raise(e)
        for x in added_values:
            del subdirectory_entries_organised[x]

    # with the subdirectories defined, we can easily get paths for everything else
    for x in flattened_config:
        required_info = x[-3:]
        paths[required_info[1]] = os.path.join(paths[required_info[0][1:]], required_info[2])


    # add to configuration object
    for k, v in paths.items():
        setattr(Configuration, k, v)


# unit test - print everything
if __name__ == "__main__":
    print("===App config===")
    for x in Configuration.App.__dict__:
        if x[0:2] != "__":
            print(x)
            for y in getattr(Configuration.App, x):
                if y[0:2] != "__":
                    value = getattr(Configuration.App, x)[y]
                    print(f"\t{y}: {value} {type(value)}")
    print("\n===Location config===")
    for x in Configuration.__dict__:
        if x[0:2] != "__":
            print(x)


