import os
import lib.filenames

IN_PROG_TAG = ".PARTIAL"


class Writer(object):
    def __init__(self, cohort, chromosome, normtype, corrtype="spearman", strength="strong"):
        self.cohort = cohort
        self.chromosome = chromosome
        self.normtype = normtype
        self.corrtype = corrtype
        self.strength = strength

        '''  if i reimplement filename metadata
        self.meta = meta

        if meta is None:
            self._filename_sp = common.file_format.correlation.format(cohort, corrtype, "strong", "positive", chromosome, norm_method)
            self._filename_sn = common.file_format.correlation.format(cohort, corrtype, "strong", "negative", chromosome, norm_method)
        else:
            self._filename_sp = common.file_format.correlation_meta.format(cohort, "strong", "positive", chromosome, norm_method, meta)
            self._filename_sn = common.file_format.correlation_meta.format(cohort, "strong", "negative", chromosome, norm_method, meta)
        '''
        self._filename_sp = lib.filenames.methylation_beta_correlation(corrtype, strength, "positive", chromosome, normtype, cohort)
        self._filename_sn = lib.filenames.methylation_beta_correlation(corrtype, strength, "negative", chromosome, normtype, cohort)

        self._sp = open(self._filename_sp + IN_PROG_TAG, "w+")
        self._sn = open(self._filename_sn + IN_PROG_TAG, "w+")

    def exit(self):
        self._sp.close()
        self._sn.close()

        # rename
        os.rename(self._filename_sp + IN_PROG_TAG, self._filename_sp)
        os.rename(self._filename_sn + IN_PROG_TAG, self._filename_sn)


    def writeCorrelation(self, positive, x, y, r_value):
        '''
        if strong:
            if positive:
                self._sp.write(f"{x},{y},{r_value}\n")
            else:
                self._sn.write(f"{x},{y},{r_value}\n")
        else:
            if positive:
                self._mp.write(f"{x},{y},{r_value}\n")
            else:
                self._mn.write(f"{x},{y},{r_value}\n")
        '''
        if positive:
            self._sp.write(f"{x},{y},{r_value}\n")
        else:
            self._sn.write(f"{x},{y},{r_value}\n")